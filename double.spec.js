const double = require("./double");

it("double of 1 is 2", () => {
  expect(double(1)).toBe(2);
});

it("double of 3 is 6", () => {
  expect(double(3)).toBe(6);
});

it("double gets error with string input", () => {
  expect(() => {
    double("a");
  }).toThrow();
    
});

it("double of 7 is 14", () => {
  expect(double(7)).toBe(14);
});
