module.exports = {
    "env": {
        "browser": true,
        "jest": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 6
    },
    "rules": {
        "indent": [
            "error",
            2
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console": 0,
        "no-undef": 0,
        "no-useless-escape": 0,
        "no-redeclare": 0,
        "no-unused-vars": 0
    }
};